const mongoose = require('mongoose')
const Product = require('../models/Product')
mongoose.connect('mongodb://localhost:27017/example')

async function cleanProduct() {
  await Product.deleteMany({})
}
async function main() {
  await cleanProduct()
  for (let i = 1; i <= 12; i++) {
    const product = new Product({ name: 'Product ' + i, price: 12000 })
    product.save()
  }
}

main().then(function () {
  console.log('Finish')
})
